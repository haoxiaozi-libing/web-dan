
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const path = require('path');

module.exports = {
    // https://cli.vuejs.org/zh/guide/webpack.html#%E9%93%BE%E5%BC%8F%E6%93%8D%E4%BD%9C-%E9%AB%98%E7%BA%A7
    chainWebpack: config => {
        config.module
            .rule('image')
            .test(/\.(png|jpe?g|gif)(\?.*)?$/)
            .use('image-webpack-loader')
            .loader('image-webpack-loader')
            .options({
                // 此处为ture的时候不会启用压缩处理,目的是为了开发模式下调试速度更快
                disable: process.env.NODE_ENV == 'development' ? true : false
            })
            .end()
    },
    // 前端自动化构建工具配置
    configureWebpack: {
        //  别名
        resolve: {
            alias: {
            '@': path.resolve(__dirname, 'src'),
            '~': path.resolve(__dirname, 'src'),
            'style': path.resolve(__dirname, 'src/style')
            }
        },
        // 优化
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    uglifyOptions: {
                        // 删除注释
                        output:{
                            comments:false
                        },
                        // 删除console debugger 
                        compress: {
                            drop_console: true,//console
                            drop_debugger: false,
                            pure_funcs: ['console.log'] //移除console
                        },
                        // 删除警告
                        warnings: false,
                    }
                })
            ]
        }
    },
    devServer: {
      proxy: {
        '/gtg': {
            target: 'http://kg.zhaodashen.cn/mt/web/',
            changeOrigin: true, // 是否允许跨域
            secure: false,      // 关闭SSL证书验证https协议接口需要改成true 
              pathRewrite: {     // 重写路径请求
                  '^/gtg': '' //路径重写 
              }
          },
          // ....
      }
    }
}

