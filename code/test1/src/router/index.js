import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  // 1 路由推荐统一小写 或者 - 或者小驼峰 
  // 2 组件名统一大驼峰
  // 3 导入就下述方式 后面项目开发完毕会有优化
  // 4 ..麻烦 可以改成@  他就代表src目录
  // { path: '/login', component: () => import('../views/Login.vue') }
  { path: '/login', component: () => import('@/views/Login.vue') },
  { path: '/todolist', component: () => import('@/views/TodoList.vue') }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

export default router
