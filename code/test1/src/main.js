// 以前：1-引入vue和vue-router、2-定义组件、3-路由、4-注册、5-挖坑显示
// 现在：思想一样，区别 模块化不要都放到一个文件 而是提取出来

// 导入Vue模块和vue-router模块
import Vue from 'vue'
// 留心：这边又把vue-router封装了下 单独搞一个文件来维护 方便后期操作
// 语法：es6 导入默认数据 
// 查找：先去当前目录找 ./router.js 找不到会自动去找 ./router/index.js
import router from './router'

// 导入App组件 跟组件  切记后期所有匹配的路由数据都会在这里面显示
import App from './App.vue'


// 开发环境 cnpm run serve  Vue 会提供很多警告来帮你对付常见的错误与陷阱。
// 但是 生产环境 cnpm run build 这些警告语句却没有用，反而会增加应用的体积。
// 其他：有些警告检查还有一些小的运行时开销，这在生产环境模式下是可以避免的
Vue.config.productionTip = true


import Swiper from '~/components/Swiper/Swiper'
Vue.use(function(Vue) {
  // Vue.component(全局组件的名字, 必须写单文件组件)
  Vue.component('qf-swiper', Swiper)
})

// Vue.use({
//   install(Vue) {
//   }
// })


new Vue({
  router, // 注册路由
  render: h => h(App) // 将单文件组件转换为虚拟DOM最终显示
}).$mount('#app')
